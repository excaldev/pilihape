/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

"use strict";
// $(document).ready(function () {
  function tooglePassword() {
    var x = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
  function loadPhoneList(storeId){
    var settings = {
      "url": "http://pilihape.test/pilihape_api/adm_ponsel/storeid/"+storeId,
      "method": "GET",
      "timeout": 0,
      "headers": {
        "Content-Type": "application/json"
      },
    };
    $.ajax(settings).done(function (response) {
      // console.log(response);
      var parsed=JSON.parse(response);
      var content='';
      parsed.forEach(e => {
        content+= `<tr>
                    <td>${e.id}</td>
                    <td>${e.merk_type}</td>
                    <td>
                        Ukuran : ${e.layar_ukuran} inch <br>
                        Resolusi : ${e.layar_resolusi} <br>
                        Model : ${e.layar_model}<br>
                    </td>
                    <td>${e.kapasitas_baterai}</td>
                    <td>${e.prosesor}</td>
                    <td>${e.ranking_prosesor}</td>
                    <td>${e.kapasitas_penyimpanan}</td>
                    <td>${e.kapasitas_ram}</td>
                    <td>${e.harga}</td>
                    <td>
                      <img src="${e.image_url}" class="img-thumbnail" >
                    </td>
                    <td>
                    <a class="btn btn-primary" >Edit</a>
                    <a class="btn btn-danger" >Hapus</a>
                    </td>
                  </tr>`
      });
      $("tbody#phone-list").html(content)
    });
  }

  $("form#form-login").submit((event) => {
    event.preventDefault();
    const username = $("#email_or_phone").val();
    const pass = $("#password").val();
    // jangan lupa diganti
    const store_id = 1;
    const credential = {
      username,
      pass,
      store_id,
    };
    login(credential);
  });
    

  const login = (user) => {
    var dataString = { user_or_phone: user.username, password: user.pass };
    var settings = {
      "url": "http://pilihape.test/pilihape_api/login",
      "method": "POST",
      "timeout": 0,
      "headers": {
        "Content-Type": "application/json",
        // "Cookie": "PHPSESSID=58sat7fsscsugoa7padt9o4pbr",
        "store_id":user.store_id
      },
      "data": JSON.stringify(dataString),
    };
    
    $.ajax(settings).done(function (response) {
      window.location.href = "dashboard.php";
      console.log(response);
    });
  };





// });
