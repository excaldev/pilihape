<!DOCTYPE html>

<?php
session_start();
$ses = @$_SESSION['user_session'];
if (!isset($ses)) {
    header('Location: login.php');
}

?>

<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- SEO Meta Tags -->
    <meta name="description" content="Situs yang bantu kamu buat milih hape sesuai kebutuhan." />
    <meta name="author" content="Mas Apry" />
    <!-- Website Title -->
    <title>
        PiliHape - Situs yang bantu kamu buat milih hape sesuai kebutuhan
    </title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Helvetica:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet" />
    <link href="css/styles.css" rel="stylesheet" />

    <!-- Favicon  -->
    <link rel="icon" href="images/favicon.png" />
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light navbar-custom fixed-top">
        <a class="navbar-brand logo-image" href="index.html"><img src="../images/logo.svg" alt="alternative" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">Add Phone</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">List Phone</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link btn-info text-white" id="logout">Logout</a>
                </li>
            </ul>
        </div>
    </nav>


    <div class="container mt-5">
        <div class="row">
            <div class="col-12 mt-5 mb-3">

            </div>
            <div class="col-12">
                <form>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="merkType">Merk Type</label>
                            <input type="text" class="form-control" id="merkType" placeholder="Merk/Type">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="layarUkuran">Layar Ukuran</label>
                            <input type="number" class="form-control" id="layarUkuran" placeholder="Layar Ukuran">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="layarResolusi">Layar Resolusi</label>
                            <select id="layarResolusi" class="form-control">
                                <option selected>Choose...</option>
                                <option value="720p">720p</option>
                                <option value="1080p">1080p</option>
                                <option value="1440p">1440p</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="layarModel">Layar Model</label>
                            <select id="layarModel" class="form-control">
                                <option selected>Choose...</option>
                                <option value="Konvensional">Konvensional</option>
                                <option value="Full View">Full View</option>
                                <option value="Notch">Notch</option>
                                <option value="Punch Hole">Punch Hole</option>
                                <option value="All Screen">All Screen</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="kapasitasBaterai">Baterai</label>
                            <input type="number" class="form-control" id="kapasitasBaterai" placeholder="Baterai">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="kameraJumlah">Jumlah Kamera</label>
                            <select id="kameraJumlah" class="form-control">
                                <option selected>Choose...</option>
                                <option value="Single">Single</option>
                                <option value="Dual">Dual</option>
                                <option value="Triple">Triple</option>
                                <option value="Quad">Quad</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="resKameraDepan">Kamera Depan</label>
                            <select id="resKameraDepan" class="form-control">
                                <option selected>Choose...</option>
                                <option value="5">5</option>
                                <option value="8">8</option>
                                <option value="12">12</option>
                                <option value="16">16</option>
                                <option value="21">21</option>
                                <option value="25">25</option>
                                <option value="32">32</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="resKameraBelakang">Kamera Belakang</label>
                            <select id="resKameraBelakang" class="form-control">
                                <option selected>Choose...</option>
                                <option value="8">8</option>
                                <option value="12">12</option>
                                <option value="16">16</option>
                                <option value="21">21</option>
                                <option value="25">25</option>
                                <option value="32">32</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Processor">Processor</label>
                            <select id="processor" class="form-control">
                            </select>
                            <input type="text" id="ranking_processor" hidden>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="kapasitasPenyimpanan">Kapasitas Penyimpanan</label>
                            <select id="kapasitasPenyimpanan" class="form-control">
                                <option selected>Choose...</option>
                                <option value="16">16</option>
                                <option value="32">32</option>
                                <option value="64">64</option>
                                <option value="128">128</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="kapasitasRam">Kapasitas RAM</label>
                            <select id="kapasitasRam" class="form-control">
                                <option selected>Choose...</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="umur">Umur</label>
                            <select id="umur" class="form-control">
                                <option selected>Choose...</option>
                                <option value="<12 bulan">&lt; 12 bulan</option>
                                <option value="12-24 bulan">12-24 bulan</option>
                                <option value=">24 bulan">&gt; 24 bulan</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="Software">Software</label>
                            <select id="software" class="form-control">
                                <option selected>Choose...</option>
                                <option value="android 6">android 6</option>
                                <option value="android 7">android 7</option>
                                <option value="android 8">android 8</option>
                                <option value="android 9">android 9</option>
                                <option value="android 10">android 10</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="kelengkapan">Kelengkapan</label>
                            <select id="kelengkapan" class="form-control">
                                <option selected>Choose...</option>
                                <option value="hanya unit">Hanya Unit</option>
                                <option value="unit dan charger">Unit dan Charger</option>
                                <option value="fullset">Fullset</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="kondisiFisik">Kondisi Fisik</label>
                            <select id="kondisiFisik" class="form-control">
                                <option selected>Choose...</option>
                                <option value="Lecet Parah">Lecet Parah</option>
                                <option value="Lecet Ringan">Lecet Ringan</option>
                                <option value="Mulus">Mulus</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="harga">Harga</label>
                            <input type="number" class="form-control" id="harga">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="storeId">Store Id</label>
                            <input type="text" class="form-control" readonly id="storeId" value="<?= $_SESSION['store_id']; ?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="image_url">Gambar</label>
                            <input type="file" class="form-control-file" id="image_url">
                        </div>
                    </div>
                    <button id="addPhone" type="submit" class="btn btn-primary">Tambah</button>
                </form>

            </div>
        </div>
    </div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->
    <script src="../js/app.js"></script>


    <script>
        $(document).ready(function() {
            var iu = $("#image_url");
            var addPhone = $('#addPhone');
            addPhone.click(function(e) {
                e.preventDefault();
                convertToBase64()
            })

            function convertToBase64() {
                if (iu[0].files && iu[0].files[0]) {
                    var FR = new FileReader();
                    FR.addEventListener("load", function(e) {
                        // document.getElementById("img").src = e.target.result;
                        // document.getElementById("b64").innerHTML = (e.target.result).split(',')[1];
                        var fileContent = (e.target.result).split(',')[1];
                        var func = sendImage(fileContent);
                        callback(func);
                    });
                    FR.readAsDataURL(iu[0].files[0]);
                }
            }

            function sendImage(file64) {
                var lg = iu[0].files.length; // get length
                var items = iu[0].files;
                var resss = "";
                if (lg > 0) {
                    for (var i = 0; i < lg; i++) {
                        var fileName = items[i].name; // get file name
                        var fileSize = items[i].size; // get file size 
                        var fileType = items[i].type; // get file type
                        var fileExt = fileName.split('.')[1];
                    }
                }
                var settings = {
                    "url": "http://pilihape.test/pilihape_api/photo-upload",
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Content-Type": "application/json"
                    },
                    "data": JSON.stringify({
                        "store_id": <?= $_SESSION['store_id']; ?>,
                        "file_name": fileName,
                        "file_ext": fileExt,
                        "file_type": fileType,
                        "file_size": fileSize,
                        "file_content": file64
                    }),
                };
                $.ajax(settings).done(function(response) {
                    var imgurl = JSON.parse(response);
                    var settings = {
                        "url": "http://pilihape.test/pilihape_api/adm_ponsel",
                        "method": "POST",
                        "timeout": 0,
                        "headers": {
                            "Content-Type": "application/json"
                        },
                        "data": JSON.stringify({
                            "id": "543",
                            "merk_type": $('#merkType').val(),
                            "layar_ukuran": $('#layarUkuran').val(),
                            "layar_resolusi": $('#layarResolusi').val(),
                            "layar_model": $('#layarModel').val(),
                            "kapasitas_baterai": $('#kapasitasBaterai').val(),
                            "kamera_jumlah": $('#kameraJumlah').val(),
                            "kamera_res_depan": $('#resKameraDepan').val(),
                            "kamera_res_belakang": $('#resKameraBelakang').val(),
                            "prosesor": $('#processor').val(),
                            "ranking_prosesor": $('#ranking_processor').val(),
                            "kapasitas_penyimpanan": $('#kapasitasPenyimpanan').val(),
                            "kapasitas_ram": $('#kapasitasRam').val(),
                            "umur": $('#umur').val(),
                            "software": $('#software').val(),
                            "kondisi_fisik": $('#kondisiFisik').val(),
                            "kelengkapan": $('#kelengkapan').val(),
                            "harga": $('#harga').val(),
                            "store_id": <?= $_SESSION['store_id']; ?>,
                            "image_url": imgurl.image_url
                        }),
                    };
                    $.ajax(settings).done(function(response) {
                        window.location.href = "dashboard.php";
                    });
                });
            }

            $.ajax({
                "url": "http://pilihape.test/pilihape_api/processor",
                "method": "GET",
                "timeout": 0,
                "headers": {},
            }).done(function(response) {
                var parsed = JSON.parse(response);
                var content = '<option selected>Choose...</option>';
                parsed.forEach(el => {
                    content += `<option value="${el.prosesor}">${el.prosesor}</option>`
                });
                $('#processor').html(content)
            });
            $('#processor').change(function() {
                var val = this.value;
                $('#ranking_processor').val(val)
            });


        });
    </script>
</body>

</html>