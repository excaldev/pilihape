<!DOCTYPE html>

<?php
session_start();
$ses = @$_SESSION['user_session'];
if (!isset($ses)) {
  header('Location: login.php');
}

?>

<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- SEO Meta Tags -->
  <meta name="description" content="Situs yang bantu kamu buat milih hape sesuai kebutuhan." />
  <meta name="author" content="Mas Apry" />
  <!-- Website Title -->
  <title>
    PiliHape - Situs yang bantu kamu buat milih hape sesuai kebutuhan
  </title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />

  <!-- Styles -->
  <link href="https://fonts.googleapis.com/css?family=Helvetica:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet" />
  <link href="css/styles.css" rel="stylesheet" />

  <!-- Favicon  -->
  <link rel="icon" href="images/favicon.png" />
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light navbar-custom fixed-top">
    <a class="navbar-brand logo-image" href="index.html"><img src="../images/logo.svg" alt="alternative" /></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="#">Add Phone</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">List Phone</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link btn-info text-white" id="logout">Logout</a>
        </li>
      </ul>
    </div>
  </nav>


  <div class="container mt-5">
    <div class="row">
      <div class="col-12 mt-5 mb-3">
        <a class="btn btn-info" href="add.php"><strong>+</strong> Add New </a>
        <?= $_SESSION['user_session'] ?>
        <?php echo '<pre>';
        var_dump($_SESSION);
        echo '</pre>'; ?>
      </div>
      <div class="col-12">
        <table class="table table-striped">
          <thead>
            <td>No</td>
            <td>Merk Type</td>
            <td>Layar</td>
            <td>Baterai</td>
            <td>Procie</td>
            <td>Rank Proc</td>
            <td>Internal</td>
            <td>RAM</td>
            <td>Harga</td>
            <td>image_url</td>
            <td>Aksi</td>
          </thead>
          <tbody id="phone-list"></tbody>
        </table>
      </div>
    </div>
  </div>


  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <!-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->
  <script src="../js/app.js"></script>


  <script>
    $(document).ready(function() {
      $("#logout").click(function() {
        var settings = {
          "url": "http://pilihape.test/pilihape_api/logout",
          "method": "POST",
        };

        $.ajax(settings).done(function(response) {
          console.log(response);
          window.location.href = "login.php";
        });
      });

      loadPhoneList(<?= $_SESSION['store_id'] ?>);
    });
  </script>
</body>

</html>